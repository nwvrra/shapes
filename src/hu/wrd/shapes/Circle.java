package hu.wrd.shapes;

public class Circle implements Shape {

    private final int radius;

    Circle(int radius) {
        if (radius <= 0)
            throw new IllegalArgumentException();

        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof Circle)) {
            return false;
        }

        Circle circle = (Circle) o;
        return this.radius == circle.radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return radius * radius * 3.14;
    }
}
