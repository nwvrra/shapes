package hu.wrd.shapes;

public interface Shape {
    public double getArea();
}
