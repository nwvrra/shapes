package hu.wrd.shapes;

public class Square implements Shape {

    private int x;

    Square(int x) {
        if (x <= 0)
            throw new IllegalArgumentException();

        this.x = x;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Square) {
            Square square = (Square) o;
            return x == square.x;
        }

        if (o instanceof Rectangle) {
            Rectangle rectangle = (Rectangle) o;
            return x == rectangle.getx() && x == rectangle.gety();
        }

        return false;
    }

    public int getx() {
        return x;
    }

    @Override
    public double getArea() {
        return x * x;
    }
}
