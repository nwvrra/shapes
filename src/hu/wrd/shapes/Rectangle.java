package hu.wrd.shapes;

public class Rectangle implements Shape {

    private int x;
    private int y;

    Rectangle(int x, int y) {
        if (x <= 0 || y <= 0)
            throw new IllegalArgumentException();

        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (o instanceof Rectangle) {
            Rectangle rectangle = (Rectangle) o;
            return
        }

        if (o instanceof Square) {
            Square square = (Square) o;
            return x == square.getx() && y == square.getx();
        }

        return false;
    }

    public int getx() {
        return x;
    }

    public int gety() {
        return y;
    }


    @Override
    public double getArea() {
        return x * y;
    }
}
