package hu.wrd.shapes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShapeService {

    List<Shape> shapes = new ArrayList<>();

    public void addShapes(Shape ... shapes) {
        var temp = new ArrayList<Shape>();

        for (Shape shape : shapes) {
            if (!this.shapes.contains(shape) && !temp.contains(shape))
                temp.add(shape);
        }

        if (shapes.length == temp.size())
            this.shapes.addAll(temp);
    }

    public void printShapesOrderByAreaAsc() {
        Collections.sort(shapes, (s1, s2) -> (int) (s1.getArea() - s2.getArea()));
        shapes.forEach(shape -> System.out.println());
    }

    public void printShapesOrderByAreaDesc() {
        //TODO implement
        System.out.println("printShapesOrderByAreaDesc() called");
    }
}
